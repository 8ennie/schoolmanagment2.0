package com.wiemann.schoolmanagment.security.api;

import com.wiemann.schoolmanagment.security.api.payload.CreateUserRequest;
import com.wiemann.schoolmanagment.security.api.payload.MessageResponse;
import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.Role;
import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.PermissionRepository;
import com.wiemann.schoolmanagment.security.repository.RoleRepository;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;

@Api(tags = "User")
@RestController
@Slf4j
@RequestMapping(path = "api/users")
public class UserApi {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;
    private final PasswordEncoder passwordEncoder;

    public UserApi(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, PermissionRepository permissionRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('USER_WRITE')")
    public ResponseEntity<?> create(@RequestBody @Valid CreateUserRequest request) {
        if (userRepository.findByUsername(request.getUsername()).isPresent()) {
            log.info("Username exists!");
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
        }

        User user = new User();
        user.setUsername(request.getUsername());
        if(request.getRoles() != null && !request.getRoles().isEmpty()) {
            user.setRoles(new HashSet<>((List<Role>) roleRepository.findAllById(request.getRoles())));
        }
        if(request.getPermissions() != null && !request.getPermissions().isEmpty()){
            user.setPermissions(new HashSet<>((List<Permission>) permissionRepository.findAllById(request.getPermissions())));
        }

        user.setPassword(passwordEncoder.encode(request.getPassword()));

        user = userRepository.save(user);

        return ResponseEntity.accepted().body(user);
    }
}
