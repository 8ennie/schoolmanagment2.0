package com.wiemann.schoolmanagment.security.domain;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Role implements GrantedAuthority {

    @Id
    private String authority;

    @ManyToMany()
    @JoinTable(
            name = "role_permissions",
            joinColumns = @JoinColumn(name = "role_authority"),
            inverseJoinColumns = @JoinColumn(name = "permissions_authority")
    )
    private Set<Permission> permissions;

    /**
     * Add a permission to the Permission set of the Role.
     *
     * @param permission the Permission to add.
     */
    public void addPermission(Permission permission){
        if(this.permissions == null){
            this.permissions = new HashSet<>();
        }
        this.permissions.add(permission);
    }

    /**
     * Remove a permission to the Permission set of the Role.
     *
     * @param permission the Permission to add.
     */
    public void removePermission(Permission permission){
        this.permissions.remove(permission);
    }
}