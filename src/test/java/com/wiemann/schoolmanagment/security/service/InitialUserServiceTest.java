package com.wiemann.schoolmanagment.security.service;


import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@SpringBootTest(properties = {"initial.user.enabled=true"})
class InitialUserServiceTest {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Test
    void givenNoUserExist_whenApplicationStartupAndPropSet_theCreateUser() {
        Optional<User> user = userRepository.findByUsername("initUser");

        Assertions.assertThat(user).isPresent();
        Assertions.assertThat(passwordEncoder.matches("initUserPassword", user.get().getPassword())).isTrue();
        Assertions.assertThat(user.get().getAuthorities().iterator().next().getAuthority()).isEqualTo("USER_WRITE");
    }
}
