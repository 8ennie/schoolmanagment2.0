package com.wiemann.schoolmanagment.security.api.payload;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;

}