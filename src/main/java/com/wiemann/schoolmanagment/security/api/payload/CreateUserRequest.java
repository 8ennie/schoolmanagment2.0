package com.wiemann.schoolmanagment.security.api.payload;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserRequest {

    @NotBlank
    @Email
    private String username;
    @NotBlank
    private String password;

    private List<String> roles;

    private List<String> permissions;

}
