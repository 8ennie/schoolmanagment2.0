package com.wiemann.schoolmanagment.security.configuration;

import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.PermissionRepository;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
class JwtTokenFilterTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JwtTokenFilter jwtTokenFilter;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    void givenValidTokenAndUserExists_whenRequestIncoming_thenSetAuthentication() throws Exception {


        Permission permission = permissionRepository.save(new Permission("USER_WRITE"));
        User user = User.builder()
                .username("test")
                .password(passwordEncoder.encode("test"))
                .permissions(Set.of(permission))
                .build();
        userRepository.save(user);
        String token = jwtTokenUtil.generateAccessToken(user);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                                "    \"username\":\"testUser\",\n" +
                                "    \"password\":\"test\"\n" +
                                "}")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isAccepted());
    }

    @Test
    void givenValidTokenAndUserNotExists_whenRequestIncoming_thenReturnUnauthorized() throws Exception {

        User user = User.builder()
                .username("test")
                .password(passwordEncoder.encode("test"))
                .build();
        String token = jwtTokenUtil.generateAccessToken(user);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                                "    \"username\":\"testUser\",\n" +
                                "    \"password\":\"test\"\n" +
                                "}")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    void givenInvalidToken_whenRequestIncoming_thenReturnUnauthorized() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                                "    \"username\":\"testUser\",\n" +
                                "    \"password\":\"test\"\n" +
                                "}")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + "aasdasd"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

}
