--liquibase formatted sql

--comment: Initial Security DDL

--changeset 8ennie:0.0.1

 create table permission (
   authority varchar(255) not null,

   primary key (authority)
);

create table role (
   authority varchar(255) not null,

   primary key (authority)
);

create table role_permissions (
   role_authority varchar(255) not null,
   permissions_authority varchar(255) not null,

   primary key (role_authority, permissions_authority)
);

create table user (
   id varchar(255) not null,
   created_at timestamp,
   enabled boolean,
   modified_at timestamp,
   password varchar(255),
   username varchar(255),

   primary key (id)
);


create table user_permissions (
   user_id varchar(255) not null,
   permissions_authority varchar(255) not null,

   primary key (user_id, permissions_authority)
);


create table user_roles (
   user_id varchar(255) not null,
   roles_authority varchar(255) not null,

   primary key (user_id, roles_authority)
);


alter table role_permissions
   add constraint FKfj6bgw3s7r23dv8553l5ga9ka
   foreign key (permissions_authority)
   references permission;


alter table role_permissions
   add constraint FK8m81qsr64jclr2i7m04amkvxh
   foreign key (role_authority)
   references role;


alter table user_permissions
   add constraint FKa3ti9og9bruiiec5jqsao6sej
   foreign key (permissions_authority)
   references permission;

alter table user_permissions
   add constraint FK79uqaq5t8qjak65ldagkoo7yr
   foreign key (user_id)
   references user;

alter table user_roles
   add constraint FKeokay9fgfkgougdcglamo2dr5
   foreign key (roles_authority)
   references role;

alter table user_roles
   add constraint FK55itppkw3i07do3h7qoclqd4k
   foreign key (user_id)
   references user;