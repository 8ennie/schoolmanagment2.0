package com.wiemann.schoolmanagment.security.repository;

import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class RoleRepositoryIntegrationTest {

    @Autowired
    private RoleRepository repository;

    @Autowired
    private PermissionRepository permissionRepository;

    @BeforeEach
    void cleanDatabase() {
        repository.deleteAll();
    }

    @Test
    void saveRole_test() {
        Permission permission1 = permissionRepository.save(new Permission("Test_P"));

        Role role = new Role("TEST", Set.of(permission1));
        Role savedRole = repository.save(role);

        Optional<Role> result = repository.findById(savedRole.getAuthority());
        assertThat(result).isPresent();
    }

    @Test
    void findAllRole_test() {
        Permission permission1 = permissionRepository.save(new Permission("Test_P"));

        Role role1 = new Role("TEST1", Set.of(permission1));
        Role role2 = new Role("TEST2", Set.of(permission1));
        Role role3 = new Role("TEST3", Set.of(permission1));
        repository.save(role1);
        repository.save(role2);
        repository.save(role3);


        List<Role> result = (List<Role>) repository.findAll();
        assertThat(result.size()).isEqualTo(3);
    }

    @Test
    void deleteRole_test() {
        Permission permission1 = permissionRepository.save(new Permission("Test_P"));

        Role role1 = new Role();
        role1.setAuthority("TEST1");
        role1.addPermission(permission1);
        Role role2 = new Role("TEST2", Set.of(permission1));
        Role role3 = new Role("TEST3", Set.of(permission1));
        repository.save(role1);
        repository.save(role2);
        Role entityToDelete = repository.save(role3);

        List<Role> result = (List<Role>) repository.findAll();
        assertThat(result.size()).isEqualTo(3);

        repository.delete(entityToDelete);

        result = (List<Role>) repository.findAll();
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    void updateRole_test() {
        Permission permission1 = permissionRepository.save(new Permission("Test_P"));
        Permission permission2 = permissionRepository.save(new Permission("Test_P2"));

        Role role3 = new Role("TEST3", Set.of(permission1));

        Role entityToUpdate = repository.save(role3);
        entityToUpdate.removePermission(permission1);
        entityToUpdate.addPermission(permission2);

        repository.save(entityToUpdate);

        Optional<Role> result = repository.findById(entityToUpdate.getAuthority());
        assertThat(result).isPresent();
        assertThat(result.get().getPermissions().iterator().next()).isEqualTo(permission2);
    }
}
