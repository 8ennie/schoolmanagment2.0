package com.wiemann.schoolmanagment.security.repository;

import com.wiemann.schoolmanagment.security.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository repository;

    @Test
    void saveUser_test() {
        User user = User.builder().username("Maxi").password("Test123").build();
        User savedUser = repository.save(user);

        Optional<User> result = repository.findById(savedUser.getId());
        assertThat(result).isPresent();
    }

    @Test
    void findAllUsers_test() {

        User user1 = User.builder().username("Maxi").password("Test123").build();
        User user2 = User.builder().username("Bob").password("Test123").build();
        User user3 = User.builder().username("Larry").password("Test123").build();
        repository.save(user1);
        repository.save(user2);
        repository.save(user3);


        List<User> result = repository.findAll();
        assertThat(result.size()).isEqualTo(3);
    }

    @Test
    void deleteUser_test() {

        User user1 = User.builder().username("Maxi").password("Test123").build();
        User user2 = User.builder().username("Bob").password("Test123").build();
        User user3 = User.builder().username("Larry").password("Test123").build();
        repository.save(user1);
        repository.save(user2);
        User entityToDelete = repository.save(user3);

        List<User> result = repository.findAll();
        assertThat(result.size()).isEqualTo(3);

        repository.delete(entityToDelete);

        result = repository.findAll();
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    void updateUser_test() {
        User user3 = User.builder().username("Larry").password("Test123").build();

        User entityToUpdate = repository.save(user3);
        entityToUpdate.setUsername("Marta");

        repository.save(entityToUpdate);

        Optional<User> result = repository.findById(entityToUpdate.getId());
        assertThat(result).isPresent();
        assertThat(result.get().getUsername()).isEqualTo("Marta");
    }

    @Test
    void findByUsername_test() {
        User user3 = User.builder().username("Larry").password("Test123").build();

        User savedUser = repository.save(user3);

        Optional<User> result = repository.findByUsername(user3.getUsername());
        assertThat(result).isPresent();
        assertThat(result.get().getUsername()).isEqualTo(user3.getUsername());
        assertThat(result.get().getId()).isEqualTo(savedUser.getId());
    }
}
