--liquibase formatted sql

--comment: Initial Permissions and Roles

--changeset 8ennie:0.0.2

INSERT INTO permission (authority) VALUES ('PERSON_READ');
INSERT INTO permission (authority) VALUES ('PERSON_WRITE');

INSERT INTO permission (authority) VALUES ('USER_WRITE');
INSERT INTO permission (authority) VALUES ('USER_READ');

INSERT INTO role (authority) VALUES ('ADMIN');
INSERT INTO role_permissions (role_authority, permissions_authority) VALUES ('ADMIN', 'USER_WRITE');
INSERT INTO role_permissions (role_authority, permissions_authority) VALUES ('ADMIN', 'USER_READ');
INSERT INTO role_permissions (role_authority, permissions_authority) VALUES ('ADMIN', 'PERSON_WRITE');
INSERT INTO role_permissions (role_authority, permissions_authority) VALUES ('ADMIN', 'PERSON_READ');
