package com.wiemann.schoolmanagment.security.service;

import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.PermissionRepository;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "initial.user", name = "enabled")
public class InitialUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @EventListener(ApplicationReadyEvent.class)
    public void createInitalUser() {
        String permission = "USER_WRITE";
        if(permissionRepository.findById(permission).isEmpty()){
            permissionRepository.save(new Permission(permission));
        }

        if (userRepository.findAll().isEmpty()) {
            User initialUser = new User();
            initialUser.setUsername("initUser");
            initialUser.setPassword(passwordEncoder.encode("initUserPassword"));

            Set<Permission> permissions = new HashSet<>();
            permissions.add(new Permission(permission));
            initialUser.setPermissions(permissions);

            userRepository.save(initialUser);
            log.warn("Initial User Created! This should not be used on a production environment");
        }


    }

}
