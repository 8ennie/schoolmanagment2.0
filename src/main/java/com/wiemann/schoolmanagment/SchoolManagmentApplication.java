package com.wiemann.schoolmanagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication()
@EnableJpaRepositories()
@EnableAutoConfiguration()
public class SchoolManagmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchoolManagmentApplication.class, args);
    }
}
