package com.wiemann.schoolmanagment.security.configuration;

import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.Role;
import com.wiemann.schoolmanagment.security.domain.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Set;

@SpringBootTest
class JwtTokenUtilTest {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private String token;

    @BeforeEach
    private void generateToken() {
        User user = User.builder()
                .id("testUserId")
                .username("test")
                .password("test")
                .roles(Set.of(Role.builder().authority("test_r").permissions(Set.of(new Permission("test_p_r"))).build()))
                .permissions(Set.of(new Permission("test_p"))).build();
        token = jwtTokenUtil.generateAccessToken(user);
        Assertions.assertThat(token).isNotNull();
    }

    @Test
    void givenValidToken_whenTokenGenerated_thenReturnToken() {
        Assertions.assertThat(token).isNotNull();
    }

    @Test
    void givenValidToken_whenUserIdRetrieved_thenReturnUserId() {
        String userId = jwtTokenUtil.getUserId(token);
        Assertions.assertThat(userId).isEqualTo("testUserId");
    }

    @Test
    void givenValidToken_whenUsernameRetrieved_thenReturnUsername() {
        String userId = jwtTokenUtil.getUsername(token);
        Assertions.assertThat(userId).isEqualTo("test");
    }

    @Test
    void givenValidToken_whenExpirationDateRetrieved_thenReturnDateInFuture() {
        Date expirationDate = jwtTokenUtil.getExpirationDate(token);
        Assertions.assertThat(expirationDate).isAfter(new Date());
    }

    @Test
    void givenValidToken_whenVerified_thenReturnTrue() {
        boolean valid = jwtTokenUtil.validate(token);
        Assertions.assertThat(valid).isTrue();
    }

    @Test
    void givenInvalidToken_whenVerified_thenReturnTrue() {
        boolean valid = jwtTokenUtil.validate("aacascaca");
        Assertions.assertThat(valid).isFalse();
    }

}
