package com.wiemann.schoolmanagment.security.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor
public class Permission implements GrantedAuthority {

    @Id
    private String authority;

    @ManyToMany(mappedBy = "permissions")
    private List<Role> roles;

    public Permission(String authority) {
        this.authority = authority;
    }

}