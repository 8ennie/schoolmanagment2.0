package com.wiemann.schoolmanagment.security.api;

import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.Role;
import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.PermissionRepository;
import com.wiemann.schoolmanagment.security.repository.RoleRepository;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Set;


@SpringBootTest
@AutoConfigureMockMvc
class AuthApiTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private UserRepository userRepository;


    @Test
    @WithMockUser(authorities = "USER_WRITE")
    void givenUserDoesExists_whenUserLogin_theValidTokenReturned()
            throws Exception {

        Permission testPermission = permissionRepository.save(new Permission("TEST"));
        Role testRole = roleRepository.save(Role.builder().authority("TEST").permissions(Set.of(testPermission)).build());
        userRepository.deleteAll();
        userRepository.save(User.builder().username("test").password(passwordEncoder.encode("test")).roles(Set.of(testRole)).build());

        this.mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/public/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"username\":\"test\",\n" +
                                "    \"password\":\"test\"\n" +
                                "}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username", Matchers.is("test")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles[0]", Matchers.is("TEST")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.permissions[0]", Matchers.is("TEST")));
    }

    @Test
    @WithMockUser(authorities = "USER_WRITE")
    void givenUserDoesExistsButWrongPassword_whenUserLoin_theReturnBadRequest()
            throws Exception {

        userRepository.save(User.builder().username("test").password(passwordEncoder.encode("test")).build());

        this.mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/public/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"username\":\"test\",\n" +
                                "    \"password\":\"admin\"\n" +
                                "}"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @AfterEach()
    void cleanUpDatabase() {
        userRepository.deleteAll();
    }

}
