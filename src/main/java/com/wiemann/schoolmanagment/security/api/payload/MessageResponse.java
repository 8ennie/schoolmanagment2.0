package com.wiemann.schoolmanagment.security.api.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MessageResponse {
	private String message;
}