package com.wiemann.schoolmanagment.security.repository;


import com.wiemann.schoolmanagment.security.domain.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, String> {
}
