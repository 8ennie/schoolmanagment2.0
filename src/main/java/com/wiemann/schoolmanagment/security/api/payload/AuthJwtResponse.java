package com.wiemann.schoolmanagment.security.api.payload;


import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class AuthJwtResponse {
    private String token;
    private String type;
    private String id;
    private String username;
    private String email;
    private List<String> roles;
    private List<String> permissions;

}
