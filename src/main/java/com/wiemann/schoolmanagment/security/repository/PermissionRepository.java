package com.wiemann.schoolmanagment.security.repository;


import com.wiemann.schoolmanagment.security.domain.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, String> {
}
