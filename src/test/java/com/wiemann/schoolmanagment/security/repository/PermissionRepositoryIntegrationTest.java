package com.wiemann.schoolmanagment.security.repository;

import com.wiemann.schoolmanagment.security.domain.Permission;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class PermissionRepositoryIntegrationTest {

    @Autowired
    private PermissionRepository permissionRepository;

    @BeforeEach
    void cleanDatabase() {
        permissionRepository.deleteAll();
    }

    @Test
    void savePermission_test() {
        Permission permission = permissionRepository.save(new Permission("Test_P"));

        Permission savedPermission = permissionRepository.save(permission);

        Optional<Permission> result = permissionRepository.findById(savedPermission.getAuthority());
        assertThat(result).isPresent();
    }

    @Test
    void findAllPermission_test() {
        permissionRepository.save(new Permission("Test_P"));
        permissionRepository.save(new Permission("Test_P1"));
        permissionRepository.save(new Permission("Test_P2"));



        List<Permission> result = (List<Permission>) permissionRepository.findAll();
        assertThat(result.size()).isEqualTo(3);
    }

    @Test
    void deletePermission_test() {
        permissionRepository.save(new Permission("Test_P"));
        permissionRepository.save(new Permission("Test_P1"));
        Permission permission3 = permissionRepository.save(new Permission("Test_P2"));

        List<Permission> result = (List<Permission>) permissionRepository.findAll();
        assertThat(result.size()).isEqualTo(3);

        permissionRepository.delete(permission3);

        result = (List<Permission>) permissionRepository.findAll();
        assertThat(result.size()).isEqualTo(2);
    }

}
