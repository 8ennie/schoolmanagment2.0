package com.wiemann.schoolmanagment.security.api;

import com.wiemann.schoolmanagment.security.api.payload.AuthJwtResponse;
import com.wiemann.schoolmanagment.security.configuration.JwtTokenUtil;
import com.wiemann.schoolmanagment.security.api.payload.AuthRequest;
import com.wiemann.schoolmanagment.security.domain.Role;
import com.wiemann.schoolmanagment.security.domain.User;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "Authentication")
@RestController
@RequestMapping(path = "api/public")
@Slf4j
public class AuthApi {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;

    public AuthApi(AuthenticationManager authenticationManager,
                   JwtTokenUtil jwtTokenUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping("login")
    public ResponseEntity<AuthJwtResponse> login(@Valid @RequestBody AuthRequest request) {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(
                            new UsernamePasswordAuthenticationToken(
                                    request.getUsername(), request.getPassword()
                            )
                    );

            User user = (User) authenticate.getPrincipal();
            AuthJwtResponse authJwtResponse = AuthJwtResponse.builder()
                    .username(user.getUsername())
                    .roles(user.getRoles().stream().map(Role::getAuthority).collect(Collectors.toList()))
                    .permissions(user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                    .id(user.getId())
                    .token(jwtTokenUtil.generateAccessToken(user))
                    .type("Bearer")
                    .build();
            return ResponseEntity.ok()
                    .header(
                            HttpHeaders.AUTHORIZATION,
                            jwtTokenUtil.generateAccessToken(user)
                    )
                    .body(authJwtResponse);
        } catch (BadCredentialsException ex) {
            log.error(ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

}