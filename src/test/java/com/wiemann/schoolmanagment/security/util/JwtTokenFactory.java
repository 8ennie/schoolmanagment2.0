package com.wiemann.schoolmanagment.security.util;

import com.wiemann.schoolmanagment.security.configuration.JwtTokenUtil;
import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.PermissionRepository;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class JwtTokenFactory {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    public String getTokenForUsernameWithPermissions(String username, Set<Permission> permissions) {

        permissionRepository.saveAll(permissions);
        User user = userRepository.findByUsername(username).orElse(null);
        if (user == null) {
            user = User.builder()
                    .username(username)
                    .build();
        }
        user.setPermissions(permissions);
        User savedUser = userRepository.save(user);

        return jwtTokenUtil.generateAccessToken(savedUser);
    }

}
