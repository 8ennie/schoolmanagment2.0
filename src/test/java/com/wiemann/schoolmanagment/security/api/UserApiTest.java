package com.wiemann.schoolmanagment.security.api;

import com.wiemann.schoolmanagment.security.domain.Permission;
import com.wiemann.schoolmanagment.security.domain.Role;
import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.PermissionRepository;
import com.wiemann.schoolmanagment.security.repository.RoleRepository;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class UserApiTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private UserRepository userRepository;



    @Test
    @WithMockUser(authorities = "USER_WRITE")
    void givenUserDoesNotExists_whenUserIsCreated_theUserReturned()
            throws Exception {


        permissionRepository.save(new Permission("TEST"));
        roleRepository.save(Role.builder().authority("TEST").build());


        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/users").contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"username\":\"test\",\n" +
                        "    \"password\":\"test\",\n" +
                        "    \"permissions\":[\"TEST\"],\n" +
                        "    \"roles\":[\"TEST\"]\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username", Matchers.is("test")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles[0].authority", Matchers.is("TEST")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.permissions[0].authority", Matchers.is("TEST")));
    }

    @Test
    @WithMockUser(authorities = "USER_WRITE")
    void givenUserAlreadyExists_whenUserIsCreated_theReturnBadRequest()
            throws Exception {

        userRepository.save(User.builder().username("test").build());

        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/users").contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "    \"username\":\"test\",\n" +
                        "    \"password\":\"test\",\n" +
                        "    \"roles\":[\"TEST\"]\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Error: Username is already taken!")));
    }

    @AfterEach()
    void cleanUpDatabase() {
        userRepository.deleteAll();
    }

}
