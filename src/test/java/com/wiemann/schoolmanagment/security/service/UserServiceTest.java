package com.wiemann.schoolmanagment.security.service;


import com.wiemann.schoolmanagment.security.domain.User;
import com.wiemann.schoolmanagment.security.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


@SpringBootTest()
class UserServiceTest {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UserService userService;

    @Test
    void givenNoUserExist_whenUserLoad_theThrowException() {
        Exception exception = Assertions.assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername("test2345"));

        String expectedMessage = "User with username - test2345, not found";
        String actualMessage = exception.getMessage();
        org.assertj.core.api.Assertions.assertThat(expectedMessage).isEqualTo(actualMessage);

    }

    @Test
    void givenUserExist_whenUserLoad_theReturnUser() {
        userRepository.deleteAll();
        User user = userRepository.save(User.builder().username("test").password("test").build());
        UserDetails userDetails = userService.loadUserByUsername("test");
        org.assertj.core.api.Assertions.assertThat(user).isEqualTo(userDetails);
    }
}
